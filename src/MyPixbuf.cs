﻿/*
 * This file is part of ChronoJump
 *
 * Chronojump is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or   
 *    (at your option) any later version.
 *    
 * Chronojump is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *    GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Copyright (C) 2024   Yang Dejiu <joeries.young@gmail.com>
 *  Copyright (C) 2024-2025  Xavier de Blas <xaviblas@gmail.com>
 */

using Gdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Chronojump
{
    /// <summary>
    /// A custom class MyPixbuf to ignore images issues
    /// </summary>
    internal static class MyPixbuf
    {
        /// <summary>
        /// Get Pixbuf by assembly name and resource path
        /// </summary>
        /// <param name="assembly">Assembly name</param>
        /// <param name="resource">Resource path</param>
        /// <returns>Return null if doesn't exist</returns>
        public static Pixbuf Get(Assembly assembly, string resource)
        {
            Pixbuf pixbuf = null;
            try
            {
                if (existsOnResourceFix (resource))
                    pixbuf = new Pixbuf (assembly, getFromResourceFix (resource));
                else
                    pixbuf = new Pixbuf(assembly, resource);
            }
            catch (Exception ex)
            {
		    LogB.Information (string.Format (
					    "catched at MyPixbuf.Get (assembly, resource). assembly: {0}, resource: {1}, exception: {2}",
					    assembly, resource, ex));
	    }
            return pixbuf;
        }

        /// <summary>
        /// Get Pixbuf by file name
        /// </summary>
        /// <param name="filename">File name</param>
        /// <returns>Return null if doesn't exist</returns>
        public static Pixbuf Get(string filename)
        {
            Pixbuf pixbuf = null;
            try
            {
                pixbuf = new Pixbuf(filename);
            }
            catch (Exception ex)
            {
		    LogB.Information (string.Format (
					    "catched at MyPixbuf.Get (filename). filename: {0}, exception: {1}",
					    filename, ex));
	    }
            return pixbuf;
        }

		//copied from RESOURCES on src/Makefile.am removed "images/". removed "md/"
        static List<ResourceFix> resourceFix_l = new List<ResourceFix> {
			new ResourceFix ("mini/no_image.png" , "mini/no_image.png"),
			new ResourceFix ("sl-jumps.png" , "selector-jumps.png"),
			new ResourceFix ("sl-runs.png" , "selector-runs.png"),
			new ResourceFix ("sl-encoder.png" , "selector-encoder.png"),
			new ResourceFix ("sl-force.png" , "selector-force.png"),
			new ResourceFix ("sl-rt.png" , "selector-rt.png"),
			new ResourceFix ("sl-multichronopic.png" , "selector-multichronopic.png"),
			new ResourceFix ("sl-jumps-small.png" , "selector-jumps-small.png"),
			new ResourceFix ("sl-runs-small.png" , "selector-runs-small.png"),
			new ResourceFix ("sl-encoder-small.png" , "selector-encoder-small.png"),
			new ResourceFix ("sl-force-small.png" , "selector-force-small.png"),
			new ResourceFix ("sl-rt-small.png" , "selector-rt-small.png"),
			new ResourceFix ("sl-multichronopic-small.png" , "selector-multichronopic-small.png"),
			new ResourceFix ("mini/force_sensor_elastic.png" , "mini/force_sensor_elastic.png"),
			new ResourceFix ("mini/force_sensor_not_elastic.png" , "mini/force_sensor_not_elastic.png"),
			new ResourceFix ("mini/run-encoder-manual.png" , "mini/run-encoder-manual.png"),
			new ResourceFix ("mini/run-encoder-resisted.png" , "mini/run-encoder-resisted.png"),
			new ResourceFix ("b-unknown.png" , "board-unknown.png"),
			new ResourceFix ("b-jump-run.png" , "board-jump-run.png"),
			new ResourceFix ("b-encoder.png" , "board-encoder.png"),
			new ResourceFix ("b-arduino-rfid.png" , "board-arduino-rfid.png"),
			new ResourceFix ("b-arduino-force.png" , "board-arduino-force.png"),
			new ResourceFix ("b-arduino-run-encoder.png" , "board-arduino-run-encoder.png"),
			new ResourceFix ("b-accelerometer.png" , "board-accelerometer.png"),
			new ResourceFix ("b-run-wireless.png" , "board-run-wireless.png"),
			new ResourceFix ("chronojump-jumps-small.png" , "chronojump-jumps-small.png"),
			new ResourceFix ("chronojump-runs-small.png" , "chronojump-runs-small.png"),
			new ResourceFix ("chronojump-encoder-small.png" , "chronojump-encoder-small.png"),
			new ResourceFix ("chronojump-inertial.png" , "chronojump-inertial.png"),
			new ResourceFix ("ag_505.png" , "agility_505.png"),
			new ResourceFix ("mini/ag_505.png" , "mini/agility_505.png"),
			new ResourceFix ("ag_20yard.png" , "agility_20yard.png"),
			new ResourceFix ("mini/ag_20yard.png" , "mini/agility_20yard.png"),
			new ResourceFix ("ag_illinois.png" , "agility_illinois.png"),
			new ResourceFix ("mini/ag_illinois.png" , "mini/agility_illinois.png"),
			new ResourceFix ("ag_shuttle.png" , "agility_shuttle.png"),
			new ResourceFix ("mini/ag_shuttle.png" , "mini/agility_shuttle.png"),
			new ResourceFix ("ag_zigzag.png" , "agility_zigzag.png"),
			new ResourceFix ("mini/ag_zigzag.png" , "mini/agility_zigzag.png"),
			new ResourceFix ("ag_t_test.png" , "agility_t_test.png"),
			new ResourceFix ("mini/ag_t_test.png" , "mini/agility_t_test.png"),
			new ResourceFix ("ag_3l3r.png" , "agility_3l3r.png"),
			new ResourceFix ("mini/ag_3l3r.png" , "mini/agility_3l3r.png"),
			new ResourceFix ("jump_free.png" , "jump_free.png"),
			new ResourceFix ("mini/jump_free.png" , "mini/jump_free.png"),
			new ResourceFix ("jump_sj.png" , "jump_sj.png"),
			new ResourceFix ("mini/jump_sj.png" , "mini/jump_sj.png"),
			new ResourceFix ("jump_sj_l.png" , "jump_sj_l.png"),
			new ResourceFix ("mini/jump_sj_l.png" , "mini/jump_sj_l.png"),
			new ResourceFix ("jump_cmj.png" , "jump_cmj.png"),
			new ResourceFix ("mini/jump_cmj.png" , "mini/jump_cmj.png"),
			new ResourceFix ("jump_cmj_l.png" , "jump_cmj_l.png"),
			new ResourceFix ("mini/jump_cmj_l.png" , "mini/jump_cmj_l.png"),
			new ResourceFix ("jump_abk.png" , "jump_abk.png"),
			new ResourceFix ("mini/jump_abk.png" , "mini/jump_abk.png"),
			new ResourceFix ("jump_abk_l.png" , "jump_abk_l.png"),
			new ResourceFix ("mini/jump_abk_l.png" , "mini/jump_abk_l.png"),
			new ResourceFix ("jump_max.png" , "jump_max.png"),
			new ResourceFix ("mini/jump_max.png" , "mini/jump_max.png"),
			new ResourceFix ("jump_dj.png" , "jump_dj.png"),
			new ResourceFix ("mini/jump_dj.png" , "mini/jump_dj.png"),
			new ResourceFix ("jump_dj_a.png" , "jump_dj_a.png"),
			new ResourceFix ("mini/jump_dj_a.png" , "mini/jump_dj_a.png"),
			new ResourceFix ("jump_dj_inside.png" , "jump_dj_inside.png"),
			new ResourceFix ("mini/jump_dj_inside.png" , "mini/jump_dj_inside.png"),
			new ResourceFix ("jump_dj_a_inside.png" , "jump_dj_a_inside.png"),
			new ResourceFix ("mini/jump_dj_a_inside.png" , "mini/jump_dj_a_inside.png"),
			new ResourceFix ("jump_rocket.png" , "jump_rocket.png"),
			new ResourceFix ("mini/jump_rocket.png" , "mini/jump_rocket.png"),
			new ResourceFix ("jump_rj.png" , "jump_rj.png"),
			new ResourceFix ("mini/jump_rj.png" , "mini/jump_rj.png"),
			new ResourceFix ("jump_rj_in.png" , "jump_rj_in.png"),
			new ResourceFix ("mini/jump_rj_in.png" , "mini/jump_rj_in.png"),
			new ResourceFix ("jump_rj_hexagon.png" , "jump_rj_hexagon.png"),
			new ResourceFix ("mini/jump_rj_hexagon.png" , "mini/jump_rj_hexagon.png"),
			new ResourceFix ("run_simple.png" , "run_simple.png"),
			new ResourceFix ("mini/run_simple.png" , "mini/run_simple.png"),
			new ResourceFix ("run_interval.png" , "run_interval.png"),
			new ResourceFix ("mini/run_interval.png" , "mini/run_interval.png"),
			new ResourceFix ("margaria.png" , "margaria.png"),
			new ResourceFix ("mini/margaria.png" , "mini/margaria.png"),
			new ResourceFix ("gesell_dbt.png" , "gesell_dbt.png"),
			new ResourceFix ("mini/gesell_dbt.png" , "mini/gesell_dbt.png"),
			new ResourceFix ("multiChronopic.png" , "multiChronopic.png"),
			new ResourceFix ("mini/multiChronopic.png" , "mini/multiChronopic.png"),
			new ResourceFix ("run_analysis.png" , "run_analysis.png"),
			new ResourceFix ("mini/run_analysis.png" , "mini/run_analysis.png"),
			new ResourceFix ("pulse_free.png" , "pulse_free.png"),
			new ResourceFix ("mini/pulse_free.png" , "mini/pulse_free.png"),
			new ResourceFix ("pulse_custom.png" , "pulse_custom.png"),
			new ResourceFix ("mini/pulse_custom.png" , "mini/pulse_custom.png"),
			new ResourceFix ("reaction_time.png" , "reaction_time.png"),
			new ResourceFix ("mini/reaction_time.png" , "mini/reaction_time.png"),
			new ResourceFix ("reaction_time_discriminative.png" , "reaction_time_discriminative.png"),
			new ResourceFix ("mini/reaction_time_discriminative.png" , "mini/reaction_time_discriminative.png"),
			new ResourceFix ("reaction_time_2025_blue_48px.png" , "reaction_time_2025_blue_48px.png"),
			new ResourceFix ("reaction_time_2025_blue_24px.png" , "reaction_time_2025_blue_24px.png"),
			new ResourceFix ("dj-from-out.png" , "dj-from-out.png"),
			new ResourceFix ("dj-from-in.png" , "dj-from-in.png"),
			new ResourceFix ("jumps-fv.png" , "jumps-fv.png"),
			new ResourceFix ("mtgug.png" , "mtgug.png"),
			new ResourceFix ("mini/mtgug.png" , "mini/mtgug.png"),
			new ResourceFix ("take_off.png" , "take_off.png"),
			new ResourceFix ("mini/take_off.png" , "mini/take_off.png"),
			new ResourceFix ("auto-by-persons.png" , "auto-by-persons.png"),
			new ResourceFix ("auto-by-tests.png" , "auto-by-tests.png"),
			new ResourceFix ("auto-by-series.png" , "auto-by-series.png"),
			new ResourceFix ("auto-person-skip.png" , "auto-person-skip.png"),
			new ResourceFix ("auto-person-remove.png" , "auto-person-remove.png"),
			new ResourceFix ("chronopic.png" , "chronopic.png"),
			new ResourceFix ("chronopic1.jpg" , "chronopic1.jpg"),
			new ResourceFix ("mini/chronopic1.jpg" , "mini/chronopic1.jpg"),
			new ResourceFix ("chronopic2.jpg" , "chronopic2.jpg"),
			new ResourceFix ("mini/chronopic2.jpg" , "mini/chronopic2.jpg"),
			new ResourceFix ("chronopic3.jpg" , "chronopic3.jpg"),
			new ResourceFix ("mini/chronopic3.jpg" , "mini/chronopic3.jpg"),
			new ResourceFix ("modular_platform_with_chronopic.jpg" , "modular_platform_with_chronopic.jpg"),
			new ResourceFix ("mini/modular_platform_with_chronopic.jpg" , "mini/modular_platform_with_chronopic.jpg"),
			new ResourceFix ("plataforma_contactos.jpg" , "plataforma_contactos.jpg"),
			new ResourceFix ("mini/plataforma_contactos.jpg" , "mini/plataforma_contactos.jpg"),
			new ResourceFix ("infrared.jpg" , "infrared.jpg"),
			new ResourceFix ("mini/infrared.jpg" , "mini/infrared.jpg"),
			new ResourceFix ("start-24-blue.png" , "start.png"),
			new ResourceFix ("cloud_blue.png" , "cloud_blue.png"),
			new ResourceFix ("cloud_yellow.png" , "cloud_yellow.png"),
			new ResourceFix ("cloud_upload_blue.png" , "cloud_upload_blue.png"),
			new ResourceFix ("cloud_view_blue.png" , "cloud_view_blue.png"),
			new ResourceFix ("cloud_view_yellow.png" , "cloud_view_yellow.png"),
			new ResourceFix ("cloud_schema.png", "cloud_schema.png"),
			new ResourceFix ("cloud_schema_small.png", "cloud_schema_small.png"),
			new ResourceFix ("notifications_blue_1x.png" , "stock_bell.png"),
			new ResourceFix ("notifications_active_blue_1x.png" , "stock_bell_active.png"),
			new ResourceFix ("notifications_none_blue_1x.png" , "stock_bell_none.png"),
			new ResourceFix ("stock_bell_green.png" , "stock_bell_green.png"),
			new ResourceFix ("stock_bell_red.png" , "stock_bell_red.png"),
			new ResourceFix ("line_session_max.png" , "line_session_max.png"),
			new ResourceFix ("line_session_avg.png" , "line_session_avg.png"),
			new ResourceFix ("line_person_max.png" , "line_person_max.png"),
			new ResourceFix ("line_person_max_all_sessions.png" , "line_person_max_all_sessions.png"),
			new ResourceFix ("line_person_avg.png" , "line_person_avg.png"),
			new ResourceFix ("audio-volume-high.png" , "audio-volume-high.png"),
			new ResourceFix ("audio-volume-muted.png" , "audio-volume-muted.png"),
			new ResourceFix ("gpm-statistics.png" , "gpm-statistics.png"),
			new ResourceFix ("jumps-profile-pie.png" , "jumps-profile-pie.png"),
			new ResourceFix ("spreadsheet.png" , "spreadsheet.png"),
			new ResourceFix ("report_view.png" , "report_view.png"),
			new ResourceFix ("attachment_blue.png" , "image_attachment.png"),
			new ResourceFix ("grid_on_blue.png" , "image_grid_on.png"),
			new ResourceFix ("photo_camera_start_blue.png" , "image_photo_start_camera.png"),
			new ResourceFix ("photo_camera_end_blue.png" , "image_photo_end_camera.png"),
			new ResourceFix ("photo_camera_do_red.png" , "image_photo_do.png"),
			new ResourceFix ("photo_camera_preview_blue.png" , "image_photo_preview.png"),
			new ResourceFix ("audio_blue_1x.png" , "audio.png"),
			new ResourceFix ("videocam_blue_1x.png" , "videocamera_on.png"),
			new ResourceFix ("videocam_off_blue_1x.png" , "videocamera_off.png"),
			new ResourceFix ("play_arrow_blue.png" , "video_play.png"),
			new ResourceFix ("home_blue_1x.png" , "image_home.png"),
			new ResourceFix ("import_1x.png" , "import.png"),
			new ResourceFix ("import_yellow_1x.png" , "import_yellow.png"),
			new ResourceFix ("export_1x.png" , "export.png"),
			new ResourceFix ("settings_blue_1x.png" , "image_settings.png"),
			new ResourceFix ("settings_down_blue_1x.png" , "image_settings_down.png"),
			new ResourceFix ("close_blue_1x.png" , "image_close_blue.png"),
			new ResourceFix ("close_red_1x.png" , "image_close.png"),
			new ResourceFix ("close_blue_2x.png" , "image_close_big.png"),
			new ResourceFix ("power_settings_new_red_1x.png" , "image_quit.png"),
			new ResourceFix ("weight.png" , "image_weight.png"),
			new ResourceFix ("weight_mov_2x.png" , "image_weight_mov_2x.png"),
			new ResourceFix ("conical_blue_1x.png" , "image_inertia.png"),
			new ResourceFix ("conical_blue_2x.png" , "image_inertia_2x.png"),
			new ResourceFix ("conical_blue_2x_2col.png" , "image_inertia_2x_2col.png"),
			new ResourceFix ("conical_yellow_1x.png" , "image_inertia_yellow.png"),
			new ResourceFix ("timer_blue_1x.png" , "image_capture.png"),
			new ResourceFix ("timer_blue_2x.png" , "image_capture_big.png"),
			new ResourceFix ("last_page_blue_1x.png" , "finish.png"),
			new ResourceFix ("gtk-floppy.png" , "floppy.png"),
			new ResourceFix ("arrow_back_blue.png" , "arrow_back.png"),
			new ResourceFix ("cancel_red_1x.png" , "image_cancel.png"),
			new ResourceFix ("empty.png" , "image_empty.png"),
			new ResourceFix ("capturing_red_1x.png" , "image_capturing.png"),
			new ResourceFix ("capturing_blue_1x.png" , "image_capturing_blue.png"),
			new ResourceFix ("done_blue.png" , "image_done_blue.png"),
			new ResourceFix ("insert_chart_blue_1x.png" , "image_analyze.png"),
			new ResourceFix ("assignment_blue_1x.png" , "image_analyze_general.png"),
			new ResourceFix ("fast_forward_blue_1x.png" , "image_sprint.png"),
			new ResourceFix ("developer_board_blue_1x.png" , "image_chronopic_connect.png"),
			new ResourceFix ("developer_board_yellow_1x.png" , "image_chronopic_connect_yellow.png"),
			new ResourceFix ("developer_board_blue_2x.png" , "image_chronopic_connect_big.png"),
			new ResourceFix ("p_blue_1x.png" , "image_person.png"),
			new ResourceFix ("p_yellow_1x.png" , "image_person_yellow.png"),
			new ResourceFix ("p_logout_blue_1x.png" , "image_person_logout.png"),
			new ResourceFix ("persons_manage.png" , "persons_manage.png"),
			new ResourceFix ("merge.png" , "merge.png"),
			new ResourceFix ("photo_camera_blue_2x.png" , "image_no_photo.png"),
			new ResourceFix ("photo_camera_yellow_2x.png" , "image_no_photo_yellow.png"),
			new ResourceFix ("select_blue_3x.png" , "image_selected.png"),
			new ResourceFix ("cached_blue_1x.png" , "image_reload.png"),
			new ResourceFix ("cached_green_1x.png" , "image_recalculate.png"),
			new ResourceFix ("delete_blue_1x.png" , "stock_delete.png"),
			new ResourceFix ("create_new_folder_blue_2x.png" , "folder_new_big.png"),
			new ResourceFix ("create_new_folder_blue_1x.png" , "folder_new.png"),
			new ResourceFix ("create_new_folder_yellow_1x.png" , "folder_new_yellow.png"),
			new ResourceFix ("fullscreen_blue_1x.png" , "image_fullscreen.png"),
			new ResourceFix ("fullscreen_exit_blue_1x.png" , "image_fullscreen_exit.png"),
			//new ResourceFix ("fullscreen_exit_blue_1x.png" , "image_minimize.png"),
			new ResourceFix ("folder_open_blue_2x.png" , "folder_open_big.png"),
			new ResourceFix ("folder_open_blue_1x.png" , "folder_open.png"),
			new ResourceFix ("folder_open_yellow_1x.png" , "folder_open_yellow.png"),
			new ResourceFix ("folder_open_set_blue_1x.png" , "folder_open_set.png"),
			new ResourceFix ("folder_open_set_ab_blue_1x.png" , "folder_open_set_ab.png"),
			new ResourceFix ("folder_open_set_cd_blue_1x.png" , "folder_open_set_cd.png"),
			new ResourceFix ("visibility_blue_1x.png" , "image_visibility.png"),
			new ResourceFix ("p_pin_blue_1x.png" , "image_person_pin.png"),
			new ResourceFix ("face_blue_1x.png" , "image_face.png"),
			new ResourceFix ("p_add_blue_1x.png" , "image_person_add.png"),
			new ResourceFix ("group_add_blue_1x.png" , "image_group_add.png"),
			new ResourceFix ("p_outline_blue_1x.png" , "image_person_outline.png"),
			new ResourceFix ("image_group_outline.png" , "image_group_outline.png"),
			new ResourceFix ("outline_mouse_black_18dp.png" , "mouse.png"),
			new ResourceFix ("weekend_blue_1x.png" , "image_rest.png"),
			new ResourceFix ("weekend_blue_inactive_1x.png" , "image_rest_inactive.png"),
			new ResourceFix ("weekend_yellow_1x.png" , "image_rest_yellow.png"),
			new ResourceFix ("looks_zero_blue_1x.png" , "zero.png"),
			new ResourceFix ("looks_one_blue_1x.png" , "one.png"),
			new ResourceFix ("add_circle_blue_1x.png" , "image_add_test.png"),
			new ResourceFix ("all_inclusive_blue_1x.png" , "cont.png"),
			new ResourceFix ("jump_blue_1x.png" , "image_jump.png"),
			new ResourceFix ("jump_blue_2x.png" , "image_jump_2x.png"),
			new ResourceFix ("jump_simple_blue_1x.png" , "image_jump_simple.png"),
			new ResourceFix ("jump_reactive_blue_1x.png" , "image_jump_reactive.png"),
			new ResourceFix ("fall.png" , "image_fall.png"),
			new ResourceFix ("jump_air.png" , "image_jump_air.png"),
			new ResourceFix ("jump_land.png" , "image_jump_land.png"),
			new ResourceFix ("walk_blue_48px.png" , "walk_48px.png"),
			new ResourceFix ("walk_back_blue_48px.png" , "walk_back_48px.png"),
			new ResourceFix ("run_blue_2x.png" , "run_2x.png"),
			new ResourceFix ("run_back_blue_2x.png" , "run_back_2x.png"),
			new ResourceFix ("run_mov_blue_2x.png" , "run_mov_2x.png"),
			new ResourceFix ("run_blue_1x.png" , "image_run.png"),
			new ResourceFix ("run_simple_blue_1x.png" , "image_run_simple.png"),
			new ResourceFix ("run_simple_yellow_1x.png" , "image_run_simple_yellow.png"),
			new ResourceFix ("run_multiple_blue_1x.png" , "image_run_multiple.png"),
			new ResourceFix ("run_multiple_yellow_1x.png" , "image_run_multiple_yellow.png"),
			new ResourceFix ("run_photocell_blue_2x.png" , "run_photocell.png"),
			new ResourceFix ("run_track_blue_1x.png" , "run_track_distance.png"),
			new ResourceFix ("build_blue_1x.png" , "image_build_24.png"),
			new ResourceFix ("build_blue_16dp_1x.png" , "image_build_16.png"),
			new ResourceFix ("edit_blue_1x.png" , "image_edit.png"),
			new ResourceFix ("add_blue_1x.png" , "image_add.png"),
			new ResourceFix ("content_copy_blue.png" , "image_duplicate.png"),
			new ResourceFix ("info_outline_blue_1x.png" , "image_info.png"),
			new ResourceFix ("fitness_center_blue_1x.png" , "image_exercise.png"),
			new ResourceFix ("keyboard_arrow_up_blue_1x.png" , "image_up.png"),
			new ResourceFix ("keyboard_arrow_down_blue_1x.png" , "image_down.png"),
			new ResourceFix ("arrow_forward_blue.png" , "arrow_forward.png"),
			new ResourceFix ("arrow_backward_blue.png" , "arrow_backward.png"),
			new ResourceFix ("arrow_forward_red_yellow.png" , "arrow_forward_emphasis.png"),
			new ResourceFix ("inertial_rolled.png" , "inertial_rolled.png"),
			new ResourceFix ("inertial_half_rolled.png" , "inertial_half_rolled.png"),
			new ResourceFix ("inertial_extended.png" , "inertial_extended.png"),
			new ResourceFix ("calibrate.png" , "calibrate.png"),
			new ResourceFix ("folder_check_blue_1x.png" , "folder_check.png"),
			new ResourceFix ("test_inspect_blue_1x.png" , "image_test_inspect.png"),
			new ResourceFix ("zoom_in.png" , "zoom_in.png"),
			new ResourceFix ("zoom_out.png" , "zoom_out.png"),
			new ResourceFix ("zoom_in_with_text.png" , "zoom_in_with_text.png"),
			new ResourceFix ("portrait_search_blue.png" , "portrait_zoom.png"),
			new ResourceFix ("trigger_2x.png" , "image_encoder_triggers.png"),
			new ResourceFix ("trigger_no_2x.png" , "image_encoder_triggers_no.png"),
			new ResourceFix ("warning_blue.png" , "image_warning_blue.png"),
			new ResourceFix ("warning_red.png" , "image_warning_red.png"),
			new ResourceFix ("list_alt_blue.png" , "image_list.png"),
			new ResourceFix ("mail_outline_blue.png" , "image_email.png"),
			new ResourceFix ("send_blue.png" , "send_blue.png"),
			new ResourceFix ("check_box_blue.png" , "image_check.png"),
			new ResourceFix ("save_blue.png" , "save.png"),
			new ResourceFix ("new_releases_blue.png" , "new.png"),
			new ResourceFix ("menu_blue.png" , "image_menu.png"),
			new ResourceFix ("menu_book_blue.png" , "image_book.png"),
			new ResourceFix ("keyboard_blue.png" , "image_keyboard.png"),
			new ResourceFix ("more_horiz_blue.png" , "image_more_horiz.png"),
			new ResourceFix ("about.png" , "image_about.png"),
			new ResourceFix ("run_time.png" , "image_run_time.png"),
			new ResourceFix ("folders_blue.png" , "image_folders.png"),
			new ResourceFix ("folders_manage_blue.png" , "folders_manage_blue.png"),
			new ResourceFix ("folders_yellow.png" , "image_folders_yellow.png"),
			new ResourceFix ("folders_backup_blue.png" , "image_db_backup.png"),
			new ResourceFix ("help_blue.png" , "image_help_blue.png"),
			new ResourceFix ("help_yellow.png" , "image_help_yellow.png"),
			new ResourceFix ("store_blue.png" , "image_store_blue.png"),
			new ResourceFix ("store_yellow.png" , "image_store_yellow.png"),
			new ResourceFix ("store_blue_news.png" , "image_store_has_new_products.png"),
			new ResourceFix ("chronojump_icon.png" , "chronojump_icon.png"),
			new ResourceFix ("chronojump_icon_transp.png" , "chronojump_icon_transp.png"),
			new ResourceFix ("chronojump_kangaroo_icon_transp.png" , "chronojump_kangaroo_icon_transp.png"),
			new ResourceFix ("chronojump_icon_graph.png" , "chronojump_icon_graph.png"),
			new ResourceFix ("stock_right.png" , "stock_right.png"),
			new ResourceFix ("stock_right_left.png" , "stock_right_left.png"),
			new ResourceFix ("stock_up.png" , "stock_up.png"),
			new ResourceFix ("stock_up_down.png" , "stock_up_down.png"),
			new ResourceFix ("stock_down.png" , "stock_down.png"),
			new ResourceFix ("stock_inertial.png" , "stock_inertial.png"),
			new ResourceFix ("reaction_time_menu.png" , "reaction_time_menu.png"),
			new ResourceFix ("pulse_menu.png" , "pulse_menu.png"),
			new ResourceFix ("multichronopic_menu.png" , "multichronopic_menu.png"),
			new ResourceFix ("bar_relative.png" , "bar_relative.png"),
			new ResourceFix ("bar_absolute.png" , "bar_absolute.png"),
			new ResourceFix ("force_sensor_menu.png" , "force_sensor_menu.png"),
			new ResourceFix ("force_sensor_icon.png" , "force_sensor_icon.png"),
			new ResourceFix ("force_sensor_icon_yellow.png" , "force_sensor_icon_yellow.png"),
			new ResourceFix ("force_2x.png" , "force_2x.png"),
			new ResourceFix ("isometric.png" , "isometric.png"),
			new ResourceFix ("elastic.png" , "elastic.png"),
			new ResourceFix ("isometric_2x.png" , "isometric_2x.png"),
			new ResourceFix ("elastic_2x.png" , "elastic_2x.png"),
			new ResourceFix ("reaction_time_icon.png" , "reaction_time_icon.png"),
			new ResourceFix ("other_icon.png" , "other_icon.png"),
			new ResourceFix ("other_icon_yellow.png" , "other_icon_yellow.png"),
			new ResourceFix ("race_encoder_blue_1x.png" , "race_encoder_icon.png"),
			new ResourceFix ("race_encoder_yellow_1x.png" , "race_encoder_icon_yellow.png"),
			new ResourceFix ("gtk-apply.png" , "gtk-apply.png"),
			new ResourceFix ("gtk-cancel.png" , "gtk-cancel.png"),
			new ResourceFix ("gtk-new-1.png" , "gtk-new-1.png"),
			new ResourceFix ("gtk-new-plus.png" , "gtk-new-plus.png"),
			new ResourceFix ("gtk-open.png" , "gtk-open.png"),
			new ResourceFix ("gtk-open-1.png" , "gtk-open-1.png"),
			new ResourceFix ("gtk-open-plus.png" , "gtk-open-plus.png"),
			new ResourceFix ("import-csv-noheaders.png" , "import-csv-noheaders.png"),
			new ResourceFix ("import-csv-headers.png" , "import-csv-headers.png"),
			new ResourceFix ("import-csv-name-1-column.png" , "import-csv-name-1-column.png"),
			new ResourceFix ("import-csv-name-2-columns.png" , "import-csv-name-2-columns.png"),
			new ResourceFix ("first.png" , "first.png"),
			new ResourceFix ("last.png" , "last.png"),
			new ResourceFix ("left.png" , "left.png"),
			new ResourceFix ("left_cut.png" , "left_cut.png"),
			new ResourceFix ("right.png" , "right.png"),
			new ResourceFix ("right_cut.png" , "right_cut.png"),
			new ResourceFix ("link.png" , "link.png"),
			new ResourceFix ("link_off.png" , "link_off.png"),
			new ResourceFix ("filter_on.png" , "filter_on.png"),
			new ResourceFix ("filter_off.png" , "filter_off.png"),
			new ResourceFix ("calendar.png" , "calendar.png"),
			new ResourceFix ("log.png" , "log.png"),
			new ResourceFix ("chronojump-logo-2024-blue-transp-200h.png" , "chronojump-logo-2024-blue-transp-200h.png"),
			new ResourceFix ("chronojump-logo-2024-white-transp-200h.png" , "chronojump-logo-2024-white-transp-200h.png"),
			new ResourceFix ("chronojump-logo-2024-2col-bluebg-210h.png" , "chronojump-logo-2024-2col-bluebg-210h.png"),
			new ResourceFix ("chronojump_logo_2024_horizontal_blue_transp_50h.png", "chronojump_logo_2024_horizontal_blue_transp_50h.png"),
			new ResourceFix ("chronojump_logo_2024_horizontal_white_transp_50h.png", "chronojump_logo_2024_horizontal_white_transp_50h.png"),
			new ResourceFix ("chronojump-logo-transparent-40h.png" , "chronojump-logo-transparent-40h.png"),
			new ResourceFix ("chronojump-logo-2013_320.png" , "chronojump-logo-2013_320.png"),
			new ResourceFix ("chronojump-logo-2013.png" , "chronojump-logo-2013.png"),
			new ResourceFix ("muscle-concentric.png" , "muscle-concentric.png"),
			new ResourceFix ("extra-mass.png" , "extra-mass.png"),
			new ResourceFix ("muscle-excentric.png" , "muscle-excentric.png"),
			new ResourceFix ("muscle-excentric-concentric.png" , "muscle-excentric-concentric.png"),
			new ResourceFix ("laterality-both.png" , "laterality-both.png"),
			new ResourceFix ("laterality-right.png" , "laterality-right.png"),
			new ResourceFix ("laterality-left.png" , "laterality-left.png"),
			new ResourceFix ("ea-individual-current-set.png" , "encoder-analyze-individual-current-set.png"),
			new ResourceFix ("ea-individual-current-session.png" , "encoder-analyze-individual-current-session.png"),
			new ResourceFix ("ea-individual-all-sessions.png" , "encoder-analyze-individual-all-sessions.png"),
			new ResourceFix ("ea-groupal-current-session.png" , "encoder-analyze-groupal-current-session.png"),
			new ResourceFix ("ea-powerbars.png" , "encoder-analyze-powerbars.png"),
			new ResourceFix ("ea-cross.png" , "encoder-analyze-cross.png"),
			new ResourceFix ("ea-1RM.png" , "encoder-analyze-1RM.png"),
			new ResourceFix ("ea-instantaneous.png" , "encoder-analyze-instantaneous.png"),
			new ResourceFix ("ea-single.png" , "encoder-analyze-single.png"),
			new ResourceFix ("ea-side.png" , "encoder-analyze-side.png"),
			new ResourceFix ("ea-superpose.png" , "encoder-analyze-superpose.png"),
			new ResourceFix ("ea-all-set.png" , "encoder-analyze-all-set.png"),
			new ResourceFix ("ea-nmp.png" , "encoder-analyze-nmp.png"),
			new ResourceFix ("ea-eccon-together.png" , "encoder-analyze-eccon-together.png"),
			new ResourceFix ("ea-eccon-separated.png" , "encoder-analyze-eccon-separated.png"),
			new ResourceFix ("ea-position.png" , "encoder-analyze-position.png"),
			new ResourceFix ("ea-speed.png" , "encoder-analyze-speed.png"),
			new ResourceFix ("ea-accel.png" , "encoder-analyze-accel.png"),
			new ResourceFix ("ea-force.png" , "encoder-analyze-force.png"),
			new ResourceFix ("ea-power.png" , "encoder-analyze-power.png"),
			new ResourceFix ("ea-mean.png" , "encoder-analyze-mean.png"),
			new ResourceFix ("ea-max.png" , "encoder-analyze-max.png"),
			new ResourceFix ("ea-range.png" , "encoder-analyze-range.png"),
			new ResourceFix ("ea-time-to-pp.png" , "encoder-analyze-time-to-pp.png"),
			new ResourceFix ("encoder-image-pending.png" , "encoder-image-pending.png"),
			new ResourceFix ("encoder-linear.png" , "encoder-linear.png"),
			new ResourceFix ("equivalentMass.png" , "equivalentMass.png"),
			new ResourceFix ("force_exerted_projected.png" , "force_exerted_projected.png"),
			new ResourceFix ("er-fr.png" , "encoder-rotary-friction.png"),
			new ResourceFix ("er-axis.png" , "encoder-rotary-axis.png"),
			new ResourceFix ("el-blue.png" , "encoder-l-blue.png"),
			new ResourceFix ("er-fr-blue.png" , "encoder-rf-blue.png"),
			new ResourceFix ("er-axis-blue.png" , "encoder-ra-blue.png"),
			new ResourceFix ("el-free-weight.png" , "encoder-l-free-weight.png"),
			new ResourceFix ("el-free-weight-inv.png" , "encoder-l-free-weight-inv.png"),
			new ResourceFix ("el-inertial.png" , "encoder-l-inertial.png"),
			new ResourceFix ("el-on-person-weighted-moving-pulley1.png" , "encoder-l-on-person-weighted-moving-pulley1.png"),
			new ResourceFix ("el-inv-on-person-weighted-moving-pulley1.png" , "encoder-l-inv-on-person-weighted-moving-pulley1.png"),
			new ResourceFix ("el-on-person-weighted-moving-pulley2.png" , "encoder-l-on-person-weighted-moving-pulley2.png"),
			new ResourceFix ("el-inv-on-person-weighted-moving-pulley2.png" , "encoder-l-inv-on-person-weighted-moving-pulley2.png"),
			new ResourceFix ("el-on-weighted-moving-pulley.png" , "encoder-l-on-weighted-moving-pulley.png"),
			new ResourceFix ("el-inclined-plane.png" , "encoder-l-inclined-plane.png"),
			new ResourceFix ("el-inclined-plane-weight-diff-angle.png" , "encoder-l-inclined-plane-weight-diff-angle.png"),
			new ResourceFix ("el-inclined-plane-weight-diff-angle-mov-pulley.png" , "encoder-l-inclined-plane-weight-diff-angle-mov-pulley.png"),
			new ResourceFix ("el-pneumatic.png" , "encoder-l-pneumatic.png"),
			new ResourceFix ("er-fr-pulley.png" , "encoder-rf-pulley.png"),
			new ResourceFix ("er-fr-pulley-axis.png" , "encoder-rf-pulley-axis.png"),
			new ResourceFix ("er-fr-side-inertial.png" , "encoder-rf-side-inertial.png"),
			new ResourceFix ("er-fr-axis-inertial.png" , "encoder-rf-axis-inertial.png"),
			new ResourceFix ("er-fr-side-inertial-lateral.png" , "encoder-rf-side-inertial-lateral.png"),
			new ResourceFix ("er-fr-axis-inertial-lateral.png" , "encoder-rf-axis-inertial-lateral.png"),
			new ResourceFix ("er-fr-side-inertial-mov-pulley.png" , "encoder-rf-side-inertial-mov-pulley.png"),
			new ResourceFix ("er-fr-axis-inertial-mov-pulley.png" , "encoder-rf-axis-inertial-mov-pulley.png"),
			new ResourceFix ("er-fr-on-fixed-pulley-with-weighted-moving-pulley.png" , "encoder-rf-on-fixed-pulley-with-weighted-moving-pulley.png"),
			new ResourceFix ("er-axis-pulley-axis.png" , "encoder-ra-pulley-axis.png"),
			new ResourceFix ("er-axis-inertial.png" , "encoder-ra-inertial.png"),
			new ResourceFix ("er-axis-inertial-lateral.png" , "encoder-ra-inertial-lateral.png"),
			new ResourceFix ("er-axis-inertial-mov-pulley.png" , "encoder-ra-inertial-mov-pulley.png"),
			new ResourceFix ("er-axis-inertial-mov-pulley-lateral.png" , "encoder-ra-inertial-mov-pulley-lateral.png"),
			new ResourceFix ("encoder-calcule-im.png" , "encoder-calcule-im.png"),
			new ResourceFix ("er-axis-on-fixed-pulley-with-weighted-moving-pulley.png" , "encoder-ra-on-fixed-pulley-with-weighted-moving-pulley.png"),
			new ResourceFix ("inertial-start.png" , "inertial-start.png"),
			new ResourceFix ("language-r.png" , "language-r.png"),
			new ResourceFix ("language-python.png" , "language-python.png"),
			new ResourceFix ("start.wav" , "sound_can_start.wav"),
			new ResourceFix ("ok.wav" , "ok.wav"),
			new ResourceFix ("bad.wav" , "sound_bad.wav")
        };

        //public static List<ResourceFix> ResourceFix_l { get => resourceFix_l; set => resourceFix_l = value; }

        static bool existsOnResourceFix (string resourcename)
        {
            foreach (ResourceFix rf in resourceFix_l)
                if (rf.resourcename == resourcename)
                    return true;
            
            return false;
        }

        static string getFromResourceFix (string resourcename)
        {
            foreach (ResourceFix rf in resourceFix_l)
                if (rf.resourcename == resourcename)
                    return rf.filename;
            
            return "";
        }
    }


    public class ResourceFix
    {
        public string filename;
        public string resourcename;

        public ResourceFix (string filename, string resourcename)
        {
            this.filename = filename;
            this.resourcename = resourcename;
        }
    }

}

