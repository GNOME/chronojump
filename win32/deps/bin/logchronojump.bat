if not exist "%tmp%\Chronojump-logs" mkdir "%tmp%\Chronojump-logs"
cd "%tmp%\Chronojump-logs\"
if exist log_chronojump.txt (
	if exist log_chronojump_old.txt (
		del log_chronojump_old.txt
	)
	ren log_chronojump.txt log_chronojump_old.txt
)

if exist "%programfiles%\Chronojump" cd "%programfiles%\Chronojump"
if exist "%programfiles(x86)%\Chronojump" cd "%programfiles(x86)%\Chronojump"

Chronojump.exe > "%tmp%\Chronojump-logs\log_chronojump.txt" 2>&1
cd %userprofile%
