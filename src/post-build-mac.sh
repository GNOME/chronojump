#!/bin/sh
cd $(dirname "$0")
rm -rf "../package/macos/deps/share/chronojump/encoder/"
rm -rf "../package/macos/deps/share/chronojump/r-scripts/"
cp -r "../package/macos/deps/" "$1/"
mkdir -p "$1/bin/angle/"
cp -r "./angle/" "$1/bin/angle/"
mkdir -p "$1/bin/chronojump-importer/"
cp -r "./chronojump-importer/" "$1/bin/chronojump-importer/"
mkdir -p "$1/po/"
cp -r "../po/" "$1/po/"
mkdir -p "$1/share/chronojump/images/"
cp -r "../win32/chronojump_icon.ico" "$1/share/chronojump/images/chronojump_icon.ico"
cp -r "../encoder/" "$1/share/chronojump/encoder/"
cp -r "../r-scripts/" "$1/share/chronojump/r-scripts/"
mkdir -p "$1/share/doc/chronojump/"
cp -r "../manual/" "$1/share/doc/chronojump/"
rm -rf "$1/bin/chronojump-importer/Makefile.in"
rm -rf "$1/bin/chronojump-importer/Makefile.am"
rm -rf "$1/bin/chronojump-importer/Makefile"
rm -rf "$1/bin/encoder/Makefile.in"
rm -rf "$1/bin/encoder/Makefile.am"
rm -rf "$1/bin/encoder/Makefile"
rm -rf "$1/po/Makefile.in.in"
rm -rf "$1/po/Makefile.in"
rm -rf "$1/po/Makefile.am"
rm -rf "$1/po/Makefile"
rm -rf "$1/share/doc/chronojump/Makefile.in"
rm -rf "$1/share/doc/chronojump/Makefile.am"
rm -rf "$1/share/doc/chronojump/Makefile"
cp ../binariesMac/7zz $1/bin/7zz
cp ../binariesMac/ffmpeg $1/bin/ffmpeg
cp ../binariesMac/ffplay $1/bin/ffplay
cp ../images/bad.wav $1/share/chronojump/images/bad.wav
cp ../images/ok.wav $1/share/chronojump/images/ok.wav
cp ../images/start.wav $1/share/chronojump/images/start.wav
mkdir -p "$1/share/chronojump/images/sounds/beepTests"
cp ../images/sounds/beepTests/*.mp3 "$1/share/chronojump/images/sounds/beepTests/"
cp ../glade/app1.glade $1/glade/app1.glade
