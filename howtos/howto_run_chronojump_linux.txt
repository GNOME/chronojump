### For building a Linux package read "package/linux/How to Make Linux Packages.txt"


# Install needed packages
sudo apt update
sudo apt install git python3 autoconf libtool intltool pkg-config build-essential libglib2.0-dev gtk-sharp3 dotnet-sdk-7.0 r-base 7zip dos2unix
# If dotnet-sdk-7.0 is not in the repositories (Ubuntu 24.04 based distributions) , you can add it with
sudo add-apt-repository ppa:dotnet/backports
sudo apt install -y dotnet-sdk-7.0

# Download Chronojump from the repo
git clone https://gitlab.gnome.org/GNOME/chronojump.git
cd chronojump

# Substitute [chronojump path] with the real path where you cloned the repo
sh autogen.sh --prefix [chronojump path]/chronojump/package/linux/deps
####if dotnet has been run but linux datetime is in the future, when put datetime back ok, need to do again previous command in order to version be really compiled
#### If using the git code but not making the package, this dir will not exist: /package/linux/deps just do this to update version on the software:
####./autogen.sh (never to this)

Note on autogen and make. Call these only the first time, any makefile file changed, git tag changed or "package/linux/deps" deleted:
autogen
make
make install
(Note make install better without sudo if possible. If there is any folder that needs root permissions, better change the permissions of that folder)

# Grant the access to the serial port. Change "[username]" by the real unername
sudo usermod -a -G dialout [username]

# cd to src
cd src

#to ensure encoder, r-scripts folders and others are correctly copied:
dos2unix post-build-linux.sh
chmod +x post-build-linux.sh

# Execute. Change "[ChronojumpPath]" by the real path where Chronojump is installed
dotnet run --project /[ChronojumpPath]/src/Chronojump-linux.csproj

# Some distros (KDE neon) requires this other command
dotnet run -r linux-x64 --project /[ChronojumpPath]/src//Chronojump-linux.csproj --property WarningLevel=0 --no-self-contained

# On Raspberry Pi which installs Debian 12, fix it as follows if it says something like "Unable to find entry point named 'SI9dbf9d88aa001ea6'":

1.
cp package/macos/deps/System.Data.SQLite.dll src/bin/Debug/net7.0/.

2. Download specific "SQLite.Interop.dll" from https://github.com/rhubarb-geek-nz/SQLite.Interop/releases/download/1.0.118.0/SQLite.Interop-1.0.118.0-debian.12.zip;

3. Copy "SQLite.Interop.dll" to src/bin/Debug/net7.0/.

4. Run Chronojump directly ([ChronojumpPath]/src/bin/Debug/net7.0/Chronojump);

5. The overwritten "System.Data.SQLite.dll" would be reverted if execute "dotnet run"; Otherwise please put the dll files above into standalone *.csproj or post-build scripts.

6. If launch from a icon launcher, create a script with 
	#!/bin/bash
	export DOTNET_ROOT=/home/cj/.dotnet
	[ChronojumpPath]/src/bin/Debug/net7.0/Chronojump
