@echo off

if "%1"=="" (
    echo Please provide the architecture parameter like x86, x64, arm or arm64.
    pause
    exit /b 1
)
cd %~dp0
%~d0
cd ../src
dotnet publish Chronojump.csproj -p:PublishProfile=Properties\PublishProfiles\win-%1.pubxml
if %errorlevel% neq 0 (
pause
exit
)
cd ../win32
"InnoSetup\ISCC.exe" chronojump_innosetup_%1.iss
pause